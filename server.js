// el require es para traerme la funcion express que está dentro de la carpeta node_modules,
// aunque lo he instalado, necesito hacer esto para usarlo
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');

app.use(bodyParser.json());

app.listen(port);
console.log("api cambio escuchando el puerto " + port);


const userController = require('./controller/UserController');
const authController = require('./controller/AuthController');
const accountController = require('./controller/AccountController');
const movementController = require('./controller/MovementController');


// el :id es un parametro
app.post('/user',userController.CreateUserV2);

app.delete('/users/:id',userController.deleteUserV1);

app.get('/users',userController.getUsersV2);

app.get('/user/:id',userController.getUserbyIdV2);

app.post('/login',authController.loginV2);

app.post('/logout',authController.logoutV2);

app.get('/accounts/:id',accountController.getAccounts);

app.get('/movements/:iban',movementController.getMovements);

app.post('/movements',movementController.createMovement);
