const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurpg6ed/collections/";
const mLabAPIKey = "apiKey=HBuLThbtN6200gFNWTUaLnmxHe6Ap7GB";


    function getMovements(req, res) {
      console.log("GET /movements/:iban");
      var iban = req.params.iban;
      console.log("IBAN recibido : " + iban );
      var query = 'q={"iban":' + '"' + iban + '"' + '}';
      // var query = 'q={"iban":' +  iban +   '}';

      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Consulta movements");
      httpClient.get("movements?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
         if (err) {
             console.log ("paso1");
             response = {
                 "msg" : "Error obteniendo movimientos de la cuenta"
              }
              res.status(500);
            } else {
              console.log ("paso2");
               if (body.length > 0) {
                 console.log ("paso3");
                  var response = body;
               } else {
                 console.log ("paso4");
                   var response = {
                     "msg" : "Este cuenta no tiene movimientos"
                       }
                   res.status(404);
                 }
               }
                   //var response = !err ? body : {
                   //"msg" : "Error obteniendo usuario."
                   //}
             res.send(response);
             }
           );
    }

    function createMovement(req, res) {
      console.log("POST /conecta/users/:iban/alta/movements");
      //Recuperar el último movimiento para esa cuenta

          var query = 'q={"iban":' + '"' + req.body.iban + '"' +'}&s={"id": -1}';
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente created");
          console.log(query);
          httpClient.get("movements?" + query + "&" + mLabAPIKey,
          function(err, resMLab, body){
            if (body.length == 0) {
                  console.log("entro en 1");
                  var newid = 1;
            } else{
                  console.log("entro en 2");
                  var newid = body[0].id + 1;
            }
           console.log("newid:  " + newid);
           var newMovement = {
             "id" : newid,
             "iban" : req.body.iban,
             "sign" : req.body.sign,
             "amount" : req.body.amount,
             "date" : req.body.date,
             "description" : req.body.description
           }
           var httpClient = requestJson.createClient(baseMLabURL);
             console.log("Cliente created");
             httpClient.post("movements?" + mLabAPIKey, newMovement,
            function(err, resMLab, body){
              var datos = {
                "iban": req.body.iban,
                "sign": req.body.sign,
                "amount": req.body.amount,
                "balance": req.body.balance
              }
              updateBalance(datos,res);
           }
           );
           }
         );
  }

  function updateBalance(req,res) {
     console.log("updateBalance");
     console.log("req.iban" + req.iban);
     console.log("req.balance" + req.balance);
     console.log("req.amount" + req.amount);
     console.log("req.sign" + req.sign);
     if (req.sign == "+"){
         var newBalance =  parseFloat(req.balance) +  parseFloat(req.amount);
     }else{
         var newBalance =  parseFloat(req.balance) -  parseFloat(req.amount);
     }
     console.log(newBalance);

     var query = 'q={"iban":' + '"' + req.iban + '"' + '}';
     console.log("Query for put is " + query);
     var httpClient = requestJson.createClient(baseMLabURL);
     var putBody = '{"$set":{"balance":' + newBalance + '}}';
     console.log(putBody);
     httpClient.put("accounts?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
     function(errPUT, resMLabPUT, bodyPUT) {
       console.log("PUT done");
       var response = {
         "msg" : "Movimiento creado con exito",
         "balance" : newBalance
       }
       res.send(response);
     }
   );
}



module.exports.getMovements = getMovements;
module.exports.createMovement = createMovement;
module.exports.updateBalance = updateBalance;
