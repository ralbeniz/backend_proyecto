const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurpg6ed/collections/";
const mLabAPIKey = "apiKey=HBuLThbtN6200gFNWTUaLnmxHe6Ap7GB";


//                  var putBody = '{}'';

function deleteUserV1 (req,res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente created");
  httpClient.delete("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       console.log (err);
       console.log (resMLab);
    //   console.log (body);
       if (err) {
           response = {
               "msg" : "Error al dar de baja el usuario"
            }
            res.status(409);
          } else {
             if (body.length > 0) {
                var response = body[0];
             } else {
                 var response = {
                   "msg" : "Usuario no encontrado"
                     }
                 res.status(404);
               }
             }
           res.send(response);
       }
     );
}

function getUsersV2(req, res) {
       console.log("GET /apitechu/v2/users");

       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.get("user?" + mLabAPIKey,
          function(err, resMLab, body){
            var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
            }
            res.send(response);
            }
          );
        }

function getUserbyIdV2(req, res) {
       console.log("GET /apitechu/v2/users/:id");

       var id = req.params.id;
       var query = 'q={"id":' + id + '}';

       var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
          if (err) {
              response = {
                  "msg" : "Error obteniendo usuario"
               }
               res.status(500);
             } else {
                if (body.length > 0) {
                   var response = body[0];
                } else {
                    var response = {
                      "msg" : "Usuario no encontrado"
                        }
                    res.status(404);
                  }
                }
                    //var response = !err ? body : {
                    //"msg" : "Error obteniendo usuario."
                    //}
              res.send(response);
              }
            );
          }

function CreateUserV2(req,res) {
   console.log("/apitechu/v2/users");
   console.log("name es " + req.body.name);
   console.log("first_name es " + req.body.first_name);
   console.log("last_name es " + req.body.last_name);
   console.log("email es " + req.body.email);
   console.log("id es " + req.body.id);
   console.log("password es " + req.body.password);

//Recuperar el último id de usuarios de la collections

    var query = 's={"id": -1}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente created");
    console.log(query);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
     var response = !err ? body : {
       "msg" : "Error obteniendo id usuario."
     }
     var newid = response[0].id + 1;
     console.log(newid);
     var newUser = {
       "id" : newid,
       "name" : req.body.name,
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "email" : req.body.email,
       "password" : crypt.hash(req.body.password)
     }
     var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.post("user?" + mLabAPIKey, newUser,
      function(err, resMLab, body){
        console.log("Usuario creado con exito");
        console.log("body[0].id" + newid)
        createFirstAccount(newid, res);
      }
     );
     }
   );
}


function createFirstAccount(iduser,res) {
   console.log("createFirstAccount");
   console.log("iduser " + iduser)

//Recuperar un iban

    var query = 'q={"used":false}&l=1';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente created");
    console.log(query);
    httpClient.get("iban?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
     var response = !err ? body : {
       "msg" : "Error obteniendo iban."
     }
     var newIban = response[0].iban;
     var query = 'q={"iban":' + '"' + newIban + '"' + '}';
     console.log("Query for put is " + query);
     var httpClient = requestJson.createClient(baseMLabURL);
     var putBody = '{"$set":{"used":true}}';
     console.log(putBody);
     httpClient.put("iban?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
     function(errPUT, resMLabPUT, bodyPUT) {
       console.log("He marcado el iban como usado");
       var response = {
         "msg" : "Iban marcado como usado",
         "iban" : newIban
       }

     }
   );
     console.log(newIban);
     var newAccount = {
       "id" : iduser,
       "iban" : newIban,
       "balance" : 0.00
     }
     var httpClient = requestJson.createClient(baseMLabURL);
       console.log("Cliente created");
       httpClient.post("accounts?" + mLabAPIKey, newAccount,
      function(err, resMLab, body){
        console.log("Cuenta creada con exito");
        var response = {
          "msg" : "Usuario creado con exito", "id": iduser}
          res.send(response);
          res.status(200);
      }
     );
     }
   );
}


module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserbyIdV2 = getUserbyIdV2;
module.exports.CreateUserV2 = CreateUserV2;
module.exports.createFirstAccount = createFirstAccount;
