const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechurpg6ed/collections/";
const mLabAPIKey = "apiKey=HBuLThbtN6200gFNWTUaLnmxHe6Ap7GB";


function loginV2(req,res) {
  console.log("DATOS DE LOGIN RECIBIDOOOOOOOOOOOOOO");
  console.log("email recibido es   :  " + req.body.email);
  console.log("password recibida es:  " + req.body.password);

  var email = req.body.email;
  var query = 'q={"email": "' + email + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Conecto con Mongo");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
       if (err) {
           response = {
               "msg" : "Error obteniendo usuario"
            }
            res.status(500);
          } else {
            console.log ("HE ENCONTRADO EL USUARIO EN MONGO");
             if (body.length > 0) {
                var userMongo = body[0];
                console.log("MAIL_MONGO:" + userMongo.email);
                console.log("PW_MONGO  :" + userMongo.password);
                if ((userMongo != null && userMongo.email == req.body.email) &&
                 crypt.checkPassword (req.body.password, userMongo.password)){
                  console.log("LAS PWS COINCIDEN");
                  var query = 'q={"id" : ' + body[0].id +'}';
                  var putBody = '{"$set":{"logged":true}}';
                  httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
            //      res.send("Loging correcto");
                  var response = {
                    "msg" : "Loging correcto", "id" : body[0].id, "name" : body[0].name, "first_name" : body[0].first_name, "last_name" : body[0].last_name
                  }
                  console.log("termino el login");
                  res.send(response);
                  res.status(200);
                } else {
            //      res.send("Loging incorrecto");
                  var response = {
                    "msg" : "Loging incorrecto"
                  }
                 res.status(403);
                 res.send(response);
                }
             } else {
                 var response = {
                   "msg" : "Usuario no encontrado"
                     }
                 res.status(404);
                 res.send(response);
               }
             }
    //   res.send(response);
       console.log("response.msg:   " + response.msg);
       console.log("response.id:    " + response.id);
       }
     );
   }

function logoutV2(req,res) {
     console.log("POST /apitechu/v1/logoutV2");
     var id = req.body.id;
     var query = 'q={"id":' + id + '}';

     var httpClient = requestJson.createClient(baseMLabURL);
     httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body){
        if (err) {
            var response = {
                "msg" : "Error obteniendo usuario"
             }
             res.send(response);
             res.status(500);
           } else {
              if (body.length > 0) {
                 var userMongo = body[0];
                 console.log("req.body.id:" + req.body.id);
                 console.log("UserMongo.id:" + userMongo.id);
                 console.log("UserMongo.logged:" + userMongo.logged);
                 if ((userMongo != null && userMongo.id == req.body.id) && (userMongo.logged == true)){
                   var putBody = '{"$unset":{"logged":""}}';
                   httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
                   var response = {
                     "msg" : "Logout correcto"}
                   res.send(response);
                   res.status(200);
                 } else {
                   var response = {
                     "msg" : "El usuario no está logado"}
                     res.send(response);
                   }
              } else {
                  var response = {
                    "msg" : "Usuario no encontrado"
                      }
                  res.send(response);
                  res.status(404);
                }
              }
            }
          );
        }

module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
